<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareerSchool extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function students()
    {
        $this->hasMany(Student::class);
    }

    public function school()
    {
        $this->belongsTo(School::class);
    }

    public function career()
    {
        $this->belongsTo(Career::class);
    }
}
