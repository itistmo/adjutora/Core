<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

/**
 * Created by PhpStorm.
 * User: computo01
 * Date: 03/05/18
 * Time: 14:49
 */

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'matricula', 'creditos', 'promedio' , 'fecha_ingreso'. 'fecha_egreso', 'semestre', 'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function careerSchool()
    {
        return $this->belongsTo(CareerSchool::class);
    }

    public function careerSpecialty()
    {
        return $this->belongsTo(CareerSchool::class);
    }

    public function interests(){
        return $this->belongsToMany(Interest::class);
    }
}