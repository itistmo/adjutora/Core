<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'nombre_segundo',
        'apellido_paterno',
        'apellido_materno',
        'fecha_nacimineto',
        'foto',
        'email',
        'curp',
        'mss',
        'new',
        'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $casts = [
        'fecha_nacimineto' => 'date:Y-m-d',
    ];

    public function phones()//polimorfica
    {
        return $this->morphMany(Phone::class,'phoneable');
    }

    public function providers()
    {
        return $this->hasMany(Provider::class);
    }

    public function emails()
    {
        return $this->hasMany(Email::class);
    }

    public function addresses()//polimorfica
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    public function admin()
    {
        return $this->hasOne(Admin::class)
            ->withDefault([
                'status' => 'El usuario no es un Administrador o no esta vinculado',
            ]);
    }

    public function student()
    {
        return $this->hasOne(Student::class)
            ->withDefault([
                'status' => 'El usuario no es un Estudiante o no esta vinculado',
            ]);
    }

    public function data()
    {
        return $this->belongsToMany(UserData::class,'user_user_data')
            ->withPivot('status')
            ->withTimestamps();
    }

    public function jobs()
    {
        return $this->belongsToMany(Company::class,'company_user')
            ->withPivot('fecha_inicio','fecha_final','puesto','status')
            ->withTimestamps();
    }
}
