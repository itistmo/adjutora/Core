<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        $query = User::with('data.type','phones','providers', 'emails', 'admin', 'student', 'jobs','addresses');
        if ($request->has('sort')) {
            list($sortCol, $sortDir) = explode('|', $request->sort);
            $query->orderBy($sortCol, $sortDir);
        } else {
            $query->orderBy('id', 'asc');
        }

        if ($request->exists('filter')) {
            $query->where(function($q) use($request) {
                $value = "%{$request->filter}%";
                $q->where('nombre', 'like', $value)
                    ->orWhere('nombre_segundo', 'like', $value)
                    ->orWhere('apellido_paterno', 'like', $value)
                    ->orWhere('apellido_materno', 'like', $value)
                    ->orWhere('fecha_nacimiento', 'like', $value)
                    ->orWhere('email', 'like', $value)
                    ->orWhere('curp', 'like', $value)
                    ->orWhere('nss', 'like', $value);
            });
        }

        $perPage = $request->has('per_page') ? (int) request()->per_page : null;

        return response()->json($query->paginate($perPage), 200);
    }

    public function store()
    {

    }

    public function show($id)
    {
        return response()->json(User::with('data.type','phones','providers', 'emails', 'admin', 'student', 'jobs','addresses')->findOrFail($id), 200);
    }

    public function update($id)
    {

    }

    public function destroy($id)
    {
        User::destroy($id);
        return ['deleted' => true];
    }
}
