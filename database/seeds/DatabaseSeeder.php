<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        DB::table('user_data')->truncate();
        DB::table('user_user_data')->truncate();
        DB::table('user_data_types')->truncate();
        DB::table('users')->truncate();
        DB::table('school_types')->truncate();
        DB::table('schools')->truncate();
        DB::table('phone_types')->truncate();
        DB::table('addresses')->truncate();

        Schema::enableForeignKeyConstraints();

        $this->call('PhoneTypeSeeder');
        $this->call('UserDataTypeSeeder');
        $this->call('SchoolTypeSeeder');
        $this->call('UserSeeder');
    }
}
