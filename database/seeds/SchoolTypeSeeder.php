<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

/**
 * Created by PhpStorm.
 * User: computo01
 * Date: 03/05/18
 * Time: 15:31
 */

use App\{
    SchoolType, UserData, UserDataType
};
use Illuminate\Database\Seeder;

class SchoolTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolType::create(['nombre'=>'Educación Superior Tecnológica']);
        SchoolType::create(['nombre'=>'Educación Superior']);
        SchoolType::create(['nombre'=>'Educación Media-Superior']);
        SchoolType::create(['nombre'=>'Educación Media-Superior Tecnológica']);
        SchoolType::create(['nombre'=>'Educación Media']);
        SchoolType::create(['nombre'=>'Educación Basica']);
    }
}