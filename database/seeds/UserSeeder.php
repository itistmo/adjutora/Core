<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

/**
 * Created by PhpStorm.
 * User: computo01
 * Date: 03/05/18
 * Time: 15:30
 */

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin=User::create([
            'nombre' => 'Admin',
            'nombre_segundo' => 'Centro',
            'apellido_paterno' => 'de',
            'apellido_materno' => 'Computo',
            'email' => 'admin@itisoft.com',
            'password' => Hash::make('password'),
            'curp' => 'MNBV12345667FG',
            'nss' => '12345678'
        ]);
        $admin->addresses()->create([
            'linea_uno' => 'Oaxaca',
            'numero' => null,
            'numero_interior' => null,
            'colonia' => 'Monte Alban',
            'localidad' => 'Salina Cruz',
            'municipio' => 'Salina Cruz',
            'estado' => 'Oaxaca',
            'pais' => 'Mexico',
            'cp' => '70610',
        ]);
        $admin->addresses()->create([
            'linea_uno' => 'Linda Vista',
            'numero' => null,
            'numero_interior' => null,
            'colonia' => 'Monte',
            'localidad' => 'Salina Cruz',
            'municipio' => 'Salina Cruz',
            'estado' => 'Oaxaca',
            'pais' => 'Mexico',
            'cp' => '70610',
        ]);
        $admin->data()->attach([3,4]);
    }
}