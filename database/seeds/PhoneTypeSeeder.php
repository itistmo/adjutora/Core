<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

/**
 * Created by PhpStorm.
 * User: computo01
 * Date: 03/05/18
 * Time: 15:31
 */

use App\PhoneType;
use Illuminate\Database\Seeder;

class PhoneTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PhoneType::create(['nombre'=>'Movil']);
        PhoneType::create(['nombre'=>'Trabajo']);
        PhoneType::create(['nombre'=>'Casa']);
        PhoneType::create(['nombre'=>'Principal']);
        PhoneType::create(['nombre'=>'Fax Laboral']);
        PhoneType::create(['nombre'=>'Fax Personal']);
        PhoneType::create(['nombre'=>'Localizador']);
    }
}