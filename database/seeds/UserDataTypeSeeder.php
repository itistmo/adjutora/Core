<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

/**
 * Created by PhpStorm.
 * User: computo01
 * Date: 03/05/18
 * Time: 15:31
 */

use App\UserData;
use App\UserDataType;
use Illuminate\Database\Seeder;

class UserDataTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $generos=UserDataType::create([
            'nombre'=>'Genero',
            'descripcion'=> 'Genero del usuario'
        ]);

        $estadoCivil=UserDataType::create([
            'nombre'=>'Estado Civil',
            'descripcion'=> 'Estado Civil asociado al usuario'
        ]);

        $generos->data()->saveMany([
            new UserData(['nombre'=>'Masculino', 'descripcion'=> 'El usuario es del genero Masculino']),
            new UserData(['nombre'=>'Femenino', 'descripcion'=> 'El usuario es del genero Femenino']),
            new UserData(['nombre'=>'Otro', 'descripcion'=> 'Sin genero especificado']),
        ]);

        $estadoCivil->data()->saveMany([
            new UserData(['nombre'=>'Soltero', 'descripcion'=> 'El usuario es Soltero/a']),
            new UserData(['nombre'=>'Casado', 'descripcion'=> 'El usuario es Casado/a']),
            new UserData(['nombre'=>'Viudo', 'descripcion'=> 'El usuario es Viudo/a']),
            new UserData(['nombre'=>'Divorciado', 'descripcion'=> 'El usuario es Divorciado/a']),
            new UserData(['nombre'=>'Comprometido', 'descripcion'=> 'El usuario es Comprometido/a']),
        ]);
    }
}