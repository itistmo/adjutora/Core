<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

$factory->define(App\Address::class, function (Faker\Generator $faker) {
    return [
        'linea_uno' => $faker->streetAddress,
        'linea_dos' => $faker->secondaryAddress,
        'numero' => $faker->buildingNumber,
        'numero_interior' => $faker->buildingNumber,
        'colonia' => $faker->streetName,
        'localidad' => $faker->city,
        'municipio' => $faker->city,
        'estado' => $faker->state,
        'pais' => 'Mexico',
        'cp' => $faker->postcode,
    ];
});
