<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyCompanyDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_company_data', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('company_data_id')->unsigned();
            $table->foreign('company_data_id')
                ->references('id')->on('company_data')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->boolean('status')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_company_data', function (Blueprint $table) {
            $table->dropForeign('company_company_data_company_id_foreign');
            $table->dropForeign('company_company_data_company_data_id_foreign');
        });
        Schema::dropIfExists('company_company_data');
    }
}