<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_schools', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('career_id')->unsigned()->nullable();
            $table->foreign('career_id')
                ->references('id')->on('careers')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('school_id')->unsigned()->nullable();
            $table->foreign('school_id')
                ->references('id')->on('schools')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('career_schools', function (Blueprint $table) {
            $table->dropForeign('career_schools_career_id_foreign');
            $table->dropForeign('career_schools_school_id_foreign');

        });
        Schema::dropIfExists('career_schools');
    }
}
