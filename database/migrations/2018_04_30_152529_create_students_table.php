<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('matricula')->unique();
            $table->integer('creditos');
            $table->float('promedio');
            $table->date('fecha_ingreso')->nullable();
            $table->date('fecha_egreso')->nullable();
            $table->integer('semestre');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('admin_id')->unsigned();
            $table->foreign('admin_id')
                ->references('id')->on('admins')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('career_specialty_id')->unsigned()->nullable();
            $table->foreign('career_specialty_id')
                ->references('id')->on('career_specialties')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('career_school_id')->unsigned();
            $table->foreign('career_school_id')
                ->references('id')->on('career_schools')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->boolean('status')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropForeign('students_user_id_foreign');
            $table->dropForeign('students_admin_id_foreign');
            $table->dropForeign('students_career_specialty_id_foreign');
            $table->dropForeign('students_career_school_id_foreign');
        });
        Schema::dropIfExists('students');
    }
}
