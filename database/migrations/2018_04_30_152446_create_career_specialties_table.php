<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCareerSpecialtiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('career_specialties', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('specialty_id')->unsigned()->nullable();
            $table->foreign('specialty_id')
                ->references('id')->on('specialties')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->integer('career_id')->unsigned()->nullable();
            $table->foreign('career_id')
                ->references('id')->on('careers')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('career_specialties', function (Blueprint $table) {
            $table->dropForeign('career_specialties_career_id_foreign');
            $table->dropForeign('career_specialties_specialty_id_foreign');
        });
        Schema::dropIfExists('career_specialties');
    }
}
