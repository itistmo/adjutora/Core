<?php
/**
 * Copyright (c) 2018. Instituto Tecnologico del Istmo
 *
 * Autorizado en virtud de la Licencia de Apache, Versión 2.0 (la "Licencia"); se prohíbe utilizar este archivo excepto en cumplimiento de la Licencia. Podrá obtener una copia de la Licencia en:
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->group(['prefix' => 'api/v1','middleware' => 'auth:api'], function () use ($router) {
    $router->get('/users/', 'UserController@index');
    $router->post('/users/', 'UserController@store');
    $router->get('/users/{id}', 'UserController@show');
    $router->put('/users/{id}', 'UserController@update');
    $router->delete('/users/{id}', 'UserController@destroy');

    $router->get('/phones/type/', 'PhoneTypeController@index');
    $router->post('/phones/type/', 'PhoneTypeController@store');
    $router->get('/phones/type/{id}', 'PhoneTypeController@show');
    $router->put('/phones/type/{id}', 'PhoneTypeController@update');
    $router->delete('/phones/type/{id}', 'PhoneTypeController@destroy');

    $router->get('/phones/', 'PhoneController@index');
    $router->post('/phones/', 'PhoneController@store');
    $router->get('/phones/{id}', 'PhoneController@show');
    $router->put('/phones/{id}', 'PhoneController@update');
    $router->delete('/phones/{id}', 'PhoneController@destroy');
});
